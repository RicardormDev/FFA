package me.richdev.FFA;

import me.richdev.FFA.Player.GamePlayer;
import org.bukkit.scheduler.BukkitRunnable;

import java.sql.*;
import java.util.UUID;

public class SQLHandler {

    private Connection connection;
    private String host, database, username, password;
    private int port;

    public SQLHandler() {

        host = "localhost";
        port = 3306;
        database = "VolkData";
        username = "VolkMC";
        password = ")84*BLuoG^%Y<[WnLNeY!<:Q9TByu1";

    }

    public void openConenction() throws SQLException, ClassNotFoundException{
        if(connection != null && !connection.isClosed()) {
            return;
        }

        synchronized (this) {
            if(connection != null && !connection.isClosed()) {
                return;
            }

            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + this.host+ ":" + this.port + "/" + this.database, this.username, this.password);
        }
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }

    public int getID(UUID uuid) {
        int result =  0 ;

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT ID FROM PlayerData WHERE UniqueId = '" + uuid.toString() + "';");

            while (resultSet.next()) {
                result = resultSet.getInt("ID");
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }


        return result;
    }

    public String getNameOfId(int iD) {
        String result = "null";
        try {

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT name FROM PlayerData WHERE ID = " + iD + ";");

            while (resultSet.next()) {
                result = resultSet.getString("name");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public boolean playerExists(int ID) {
        int i = 0;

        try{
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT PlayerID FROM FullPVP WHERE PlayerID = " + ID + ";");

            while (resultSet.next()) {
                i = resultSet.getInt("PlayerID");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return i != 0;
    }

    public void createPlayer(int ID) {

        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO FullPVP VALUES (?,?,?,?,?,?)");

            statement.setInt(1, ID);
            statement.setInt(2, 0);
            statement.setInt(3, 0);
            statement.setInt(4, 0);
            statement.setInt(5, 0);
            statement.setInt(6, 0);

        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public void savePlayer(GamePlayer gamePlayer) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    PreparedStatement statement = connection.prepareStatement("UPDATE FullPVP SET Kills=?, Assists=?, LVL=?, XP=?, Deads=?, Gold=?, XPREST=? WHERE PlayerID = " + gamePlayer.getID() + ";");

                    statement.setInt(1, gamePlayer.getKills());
                    statement.setInt(2, gamePlayer.getAssists());
                    statement.setInt(3, gamePlayer.getLEVEL());
                    statement.setLong(4,gamePlayer.getXP());
                    statement.setInt(5, gamePlayer.getDeads());
                    statement.setInt(6, gamePlayer.getGold());
                    statement.setLong(7, gamePlayer.getXPRequiredToNewLevel());

                    statement.executeUpdate();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(Main.getInstance());
    }

    public void savePlayerNotAsync(GamePlayer gamePlayer) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE FullPVP SET Kills=?, Assists=?, LVL=?, XP=?, Deads=?, Gold=?, XPREST=? WHERE PlayerID = " + gamePlayer.getID() + ";");

            statement.setInt(1, gamePlayer.getKills());
            statement.setInt(2, gamePlayer.getAssists());
            statement.setInt(3, gamePlayer.getLEVEL());
            statement.setLong(4,gamePlayer.getXP());
            statement.setInt(5, gamePlayer.getDeads());
            statement.setInt(6, gamePlayer.getGold());
            statement.setLong(7, gamePlayer.getXPRequiredToNewLevel());

            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void loadPlayer(GamePlayer gamePlayer) {

        try {

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM FullPVP WHERE PlayerID = " + gamePlayer.getID() + ";");

            while(resultSet.next()) {

                gamePlayer.setKills(resultSet.getInt("Kills"));
                gamePlayer.setAssists(resultSet.getInt("Assists"));
                gamePlayer.setLevel(resultSet.getInt("LVL"));
                gamePlayer.setXP(resultSet.getLong("XP"));
                gamePlayer.setDeads(resultSet.getInt("Deads"));
                gamePlayer.setGold(resultSet.getInt("Gold"));
                gamePlayer.setXPRequiredToNewLevel(resultSet.getLong("XPREST"));

            }

        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
