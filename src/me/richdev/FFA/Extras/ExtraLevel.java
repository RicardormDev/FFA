package me.richdev.FFA.Extras;

public abstract class ExtraLevel extends Extra{

    private int level;
    private int maxLevel;

    public ExtraLevel(String ID, int maxLevel) {
        super(ID);
        this.maxLevel = maxLevel;
        this.level = 1;
    }

    public int getMaxLevel() {
        return maxLevel;
    }

    public int getLevel() {
        return level;
    }

    public void upgradeLevel() {
        this.level++;
    }
}
