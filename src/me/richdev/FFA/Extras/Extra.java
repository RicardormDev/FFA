package me.richdev.FFA.Extras;

public abstract class Extra {

    private String ID;
    private String displayName;
    private String[] description;

    public Extra(String ID) {
        this.ID  = ID;
        this.displayName = ID.replace("_" , " ");
    }

    public String getID() {
        return ID;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String[] getDescription() {
        return description;
    }

    public void setDescription(String[] description) {
        this.description = description;
    }

    public abstract void run();
}
