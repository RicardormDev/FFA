package me.richdev.FFA;

import me.richdev.FFA.GUIS.GuiSaver;
import me.richdev.FFA.Player.GamePlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String s, String[] args) {

        if(cmd.getName().equalsIgnoreCase("ffa")) {
            if(args.length == 0) {

            } else if(args[0].equalsIgnoreCase("nonpermament")) {
                if(!(sender instanceof Player)) {
                    sender.sendMessage("§cComando para jugadores.");
                    return true;
                }
                GuiSaver.openGui(Main.getInstance().getPlayerHandler().getPlayer((Player) sender));
            } else if(args[0].equalsIgnoreCase("admin") && sender.hasPermission("FFA.admin")) {

                if(args.length < 2) {
                    sender.sendMessage("§cComando desconocido.");
                    return true;
                }

                if(args[1].equalsIgnoreCase("oro")) {

                    if(args.length < 4) {
                        sender.sendMessage(ChatColor.RED + "Uso /ffa admin oro [jugador] [oro]");
                        return true;
                    }

                    int amount;

                    if(!args[3].matches("[0-9]+")) {

                        sender.sendMessage("§cLa cantidad de oro solo puede ser en numeros.");

                        return true;
                    }

                    amount = Integer.parseInt(args[3]);

                    GamePlayer target = Main.getInstance().getPlayerHandler().getPlayer(args[2]);

                    if(target == null) {
                        sender.sendMessage("§cEl jugador §e" + args[2] + " §cno está jugado.");
                        return true;
                    }

                    target.addGold(amount);
                    target.updateScoreboard();
                    sender.sendMessage("§eHas agregado la cantidad de §6" + amount + " §ede oro al jugador §6" + target.getName());

                } else {
                    sender.sendMessage("§cComando desconocido.");
                }

            }
        }

        return true;
    }
}
