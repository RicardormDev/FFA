package me.richdev.FFA.Player;

import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class PlayerHandler {

    private Set<GamePlayer> players;
    public PlayerHandler() {
        players = new HashSet<>();
    }

    public void registerPlayer(GamePlayer player) {
        players.add(player);
    }

    public void unregisterPlayer(GamePlayer player) {
        players.remove(player);
    }

    public GamePlayer getPlayer(Player player) {
        for (GamePlayer gamePlayer : players) {
            if(gamePlayer.getPlayer().equals(player)) {
                return gamePlayer;
            }
        }
        return null;
    }

    public GamePlayer getPlayer(String player) {
        for (GamePlayer gamePlayer : players) {
            if(gamePlayer.getName().equals(player)) {
                return gamePlayer;
            }
        }
        return null;
    }

    public Set<GamePlayer> getAll() {
        return players;
    }
}
