package me.richdev.FFA.Player;

import me.richdev.FFA.Extras.Extra;
import me.richdev.FFA.KillManager.GamePlayerDeathEvent;
import me.richdev.FFA.Main;
import me.richdev.FFA.Player.Leveling.PVPTitle;
import me.richdev.FFA.Scoreboard.ScoreboardLobby;
import me.richdev.FFA.Scoreboard.ServerScoreboard;
import me.richdev.FFA.Utils.ItemBuilder;
import me.richdev.FFA.Utils.PacketsSenders;
import me.richdev.FFA.Utils.TitleUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class GamePlayer {

    private int ID;
    private Player player;
    private String name;
    private UUID UniqueID;

    private int LEVEL;
    private long XP;
    private long XPRequiredToNewLevel;
    private PVPTitle title;

    private Extra slot1;
    private Extra slot2;
    private Extra slot3;

    private Set<Extra> misExtras;
    private Set<Extra> extrasLeveleales;

    private Set<GamePlayer> hitted;
    private GamePlayer lastHiter;

    private int kills;
    private int assists;
    private int deads;

    private int gold;

    private int killstreak;
    private int specialGoldPrice;

    private ServerScoreboard scoreboard;

    public GamePlayer (Player player, int ID) {
        this.ID = ID;
        this.player = player;
        this.name = player.getName();
        this.UniqueID = player.getUniqueId();

        this.LEVEL = 0;
        this.XP = 0;
        this.XPRequiredToNewLevel = LEVEL * 10;
        this.title = PVPTitle.RECLUTA;

        this.slot1 = null;
        this.slot2 = null;
        this.slot3 = null;

        this.misExtras = new HashSet<>();
        this.extrasLeveleales = new HashSet<>();
        this.hitted = new HashSet<>();

        this.kills = 0;
        this.assists = 0;
        this.deads = 0;

        this.gold = 0;
        this.specialGoldPrice = 0;

        Main.getInstance().getSqlHandler().loadPlayer(this);

        updateLevel();
        updateTitle();
        updateScoreboard();
    }

    private static final ItemStack[] armor = new ItemStack[]{
            new ItemBuilder().setDisplayName("").setMaterial(Material.IRON_BOOTS).addEnchant(Enchantment.DURABILITY, 10).build(),
            new ItemBuilder().setDisplayName("").setMaterial(Material.IRON_LEGGINGS).addEnchant(Enchantment.DURABILITY, 10).build(),
            new ItemBuilder().setDisplayName("").setMaterial(Material.IRON_CHESTPLATE).addEnchant(Enchantment.DURABILITY, 10).build(),
            new ItemBuilder().setDisplayName("").setMaterial(Material.IRON_HELMET).addEnchant(Enchantment.DURABILITY, 10).build(), };
    public static final ItemStack[] items = new ItemStack[]{
            new ItemBuilder().setDisplayName("").setMaterial(Material.IRON_SWORD).addEnchant(Enchantment.DURABILITY, 10).build(),
            new ItemBuilder().setDisplayName("").setMaterial(Material.BOW).addEnchant(Enchantment.DURABILITY, 10).build(),
            new ItemBuilder().setDisplayName("").setMaterial(Material.ARROW).setQuantity(32).build(),
            new ItemBuilder().setDisplayName("").setMaterial(Material.COOKED_BEEF).setQuantity(64).build(),
            new ItemBuilder().setDisplayName("").setMaterial(Material.GOLDEN_APPLE).setQuantity(1).build(),
            new ItemBuilder().setDisplayName("").setMaterial(Material.FISHING_ROD).addEnchant(Enchantment.DURABILITY, 10).setQuantity(1).build(), };

    public static final ItemStack[] special = new ItemStack[]{
            new ItemBuilder().setMaterial(Material.DIAMOND_CHESTPLATE).addEnchant(Enchantment.DURABILITY, 10).build(),
            new ItemBuilder().setMaterial(Material.DIAMOND_BOOTS).addEnchant(Enchantment.DURABILITY, 10).build(),
            new ItemBuilder().setMaterial(Material.DIAMOND_SWORD).addEnchant(Enchantment.DURABILITY, 10).build()
    };

    public static void setInventory(Player player, boolean force) {
        PlayerInventory inv = player.getInventory();

        if(!inv.contains(Material.DIAMOND_SWORD) || force)
            inv.setItem(0, items[0]);

        inv.setItem(1, items[5]);
        inv.setItem(2, items[1]);
        inv.setItem(4, items[3]);
        inv.setItem(5, items[4]);
        inv.setItem(8, items[2]);

        if(inv.getBoots().getType() != Material.DIAMOND_BOOTS || force)
            inv.setBoots(armor[0]);

        inv.setLeggings(armor[1]);

        if(inv.getChestplate().getType() != (Material.DIAMOND_CHESTPLATE) || force)
            inv.setChestplate(armor[2]);


        inv.setHelmet(armor[3]);
    }

    public void updateScoreboard() {
        new ScoreboardLobby(this);
    }

    public int getID() {
        return ID;
    }

    public Player getPlayer() {
        return player;
    }

    public String getName() {
        return name;
    }

    public UUID getUniqueID() {
        return UniqueID;
    }

    public int getLEVEL() {
        return LEVEL;
    }

    public void setLEVEL(int LEVEL) {
        this.LEVEL = LEVEL;
    }

    public long getXP() {
        return XP;
    }

    public void setXP(long XP) {
        this.XP = XP;
    }

    public Extra getSlot1() {
        return slot1;
    }

    public void setSlot1(Extra slot1) {
        this.slot1 = slot1;
    }

    public Extra getSlot2() {
        return slot2;
    }

    public int getKillstreak() {
        return killstreak;
    }

    public void setSlot2(Extra slot2) {
        this.slot2 = slot2;
    }

    public Extra getSlot3() {
        return slot3;
    }

    public void setSlot3(Extra slot3) {
        this.slot3 = slot3;
    }

    public Set<Extra> getMisExtras() {
        return misExtras;
    }

    public Set<Extra> getExtrasLeveleales() {
        return extrasLeveleales;
    }

    public Set<GamePlayer> getHitted() {
        return hitted;
    }

    public GamePlayer getLastHiter() {
        return lastHiter;
    }

    public void setLastHiter(GamePlayer lastHiter) {
        this.lastHiter = lastHiter;
    }

    public void setXPRequiredToNewLevel(long t) {
        if(t == -1) {
            t = LEVEL * 10;
        }
        this.XPRequiredToNewLevel = t;
    }

    public void addHitter(GamePlayer k) {
        if(!hitted.contains(k) || k != this) {
            hitted.add(k);
        }
    }

    public void addKill(GamePlayer dead) {
        int add = Main.getInstance().getNumberBetweenTwo(5, 15);
        int xp = Main.getInstance().getNumberBetweenTwo(10, 20);
        player.sendMessage("§b§lKILL! §eMataste a §a" + dead.getName() + " §6+" + add + " oro §b+" + xp + "XP");

        kills++;
        killstreak++;

        if(killstreak % 5 == 0) {
            Bukkit.broadcastMessage("§b§l" + name + " §etiene una racha de §b§l" + killstreak + " §ekills! Matalo para recibir una recompensa.");
            this.specialGoldPrice = killstreak * 2;
        }

        addXP(xp);
        addGold(add);

        int last = LEVEL;
        updateLevel();
        if(last != LEVEL) {
            PacketsSenders.sendTitle(getPlayer(), "§aSubiste de nivel!",  title.getColor() + String.valueOf(last) + " §7➟ " + title.getColor() + String.valueOf(LEVEL), 0, 50, 10);
        }

        updateScoreboard();
    }

    public void addAssist(GamePlayer dead) {
        int add = Main.getInstance().getNumberBetweenTwo(1, 3);
        int xp = Main.getInstance().getNumberBetweenTwo(1, 3);
        player.sendMessage("§b§lASISTENCIA! §a" + dead.getName() + "§e murió §6+" + add + " oro §b+" + xp + "XP");

        assists++;
        addXP(xp);
        addGold(add);

        int last = LEVEL;
        updateLevel();
        if(last != LEVEL) {
            PacketsSenders.sendTitle(getPlayer(), "§aSubiste de nivel!",  title.getColor() + String.valueOf(last) + " §7➟ " + title.getColor() + String.valueOf(LEVEL), 0, 50, 10);
        }
        updateScoreboard();
    }

    public void addDead() {
        deads++;
        killstreak = 0;
        specialGoldPrice = 0;
        updateScoreboard();
    }

    public void addGold(double gold) {
        this.gold += gold;
    }

    public void removeGold(double gold) {
        this.gold -= gold;
        if(this.gold < 0) this.gold = 0;
        updateScoreboard();
    }

    private void updateTitle() {
        PVPTitle currentNextTitle = TitleUtils.getNext(title);

        while(LEVEL > currentNextTitle.getLevelGet()) {
            title = currentNextTitle;
            PVPTitle checkNext = TitleUtils.getNext(currentNextTitle);
            if(checkNext == title) {
                //puedes devolver null en caso de que no haya un titulo siguiente, en el caso de que se haya usado getNext() en el ultimo titulo disponible.
                break;
            }
            currentNextTitle = checkNext;
        }

    }

    private void updateLevel() {
        //LEVEL = (int) (XP / 200);
        if(XPRequiredToNewLevel <= 0) { //(XP % 200) == 0
            LEVEL++;
            updateTitle();
            XPRequiredToNewLevel = LEVEL * 10;
        }
    }

    public int getKills() {
        return kills;
    }

    public int getDeads() {
        return deads;
    }

    public int getAssists() {
        return assists;
    }

    public int getGold() {
        return gold;
    }

    public long getXPRequiredToNewLevel() {
        return XPRequiredToNewLevel;
    }

    public PVPTitle getTitle() {
        return title;
    }

    private String c(String m ) {
        return ChatColor.translateAlternateColorCodes('&', m);
    }

    public int getSpecialGoldPrice() {
        return specialGoldPrice;
    }

    public void setKills(int a) {
        this.kills = a;
    }

    public void setDeads(int a) {
        this.deads = a;
    }

    public void setGold(int a) {
        this.gold = a;
    }

    public void setAssists(int a) {
        this.assists = a;
    }

    public void setXP(int a) {
        this.XP = a;
    }

    public void addXP(int a) {
        this.XP += a;
        XPRequiredToNewLevel -= a;
    }

    public void setLevel(int a) {
        this.LEVEL = a;
    }

    public boolean isOnline() {
        return player != null && player.isOnline();
    }

    public ServerScoreboard getScoreboard() {
        return scoreboard;
    }

    public void setScoreboard(ServerScoreboard scoreboard) {
        this.scoreboard = scoreboard;
    }
}
