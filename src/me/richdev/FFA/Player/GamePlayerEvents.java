package me.richdev.FFA.Player;

import me.richdev.FFA.Main;
import me.richdev.FFA.Utils.NameTagChanger;
import me.richdev.FFA.Utils.TeamAction;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class GamePlayerEvents implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void join(PlayerJoinEvent e) {
        GamePlayer player = Main.getInstance().getPlayerHandler().getPlayer(e.getPlayer());
        if(player == null) {

            int playerID = Main.getInstance().getSqlHandler().getID(e.getPlayer().getUniqueId());

            if(!Main.getInstance().getSqlHandler().playerExists(playerID)) {
                Main.getInstance().getSqlHandler().createPlayer(playerID);
            }

            player = new GamePlayer(e.getPlayer(), playerID);
        }

        Main.getInstance().getPlayerHandler().registerPlayer(player);
    }

    @EventHandler
    public void leave(PlayerQuitEvent e) {
        GamePlayer player = Main.getInstance().getPlayerHandler().getPlayer(e.getPlayer());
        if(player != null) {
            Main.getInstance().getSqlHandler().savePlayer(player);
            Main.getInstance().getPlayerHandler().unregisterPlayer(player);
        }
    }

}
