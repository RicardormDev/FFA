package me.richdev.FFA.KillManager;

import me.richdev.FFA.Player.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.potion.PotionEffect;

public class GamePlayerDeathEvent extends Event {

    private static final Location SPAWN = new Location(Bukkit.getWorld("Spawn"), -198.407, 87, -60.530, -92.3F, -0.4F);
    private static final HandlerList handlers = new HandlerList();

    private GamePlayer dead;
    private GamePlayer killer;

    private String killMessage;

    public GamePlayerDeathEvent(GamePlayer dead, @Nullable GamePlayer killer, @Nullable String killMessage) {
        this.dead = dead;
        this.killer = killer;
        this.killMessage = killMessage;

        dead.getPlayer().getInventory().clear();
        dead.getPlayer().teleport(SPAWN);
        dead.getPlayer().setHealth(dead.getPlayer().getMaxHealth());
        dead.getPlayer().setFoodLevel(20);
        for (PotionEffect potionEffect : dead.getPlayer().getActivePotionEffects()) {
            dead.getPlayer().removePotionEffect(potionEffect.getType());
        }

        if(killer != null) {
            killer.getPlayer().playSound(killer.getPlayer().getLocation(), Sound.ORB_PICKUP, 10, 10);
        }
        GamePlayer.setInventory(dead.getPlayer(), true);
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    public GamePlayer getDead() {
        return dead;
    }

    public GamePlayer getKiller() {
        return killer;
    }

    public String getKillMessage() {
        return killMessage;
    }

    public void setKillMessage(String killMessage) {
        this.killMessage = killMessage;
    }

}
