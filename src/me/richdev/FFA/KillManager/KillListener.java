package me.richdev.FFA.KillManager;

import me.richdev.FFA.Main;
import me.richdev.FFA.Player.GamePlayer;
import me.richdev.FFA.Player.PlayerHandler;
import me.richdev.FFA.Utils.ItemBuilder;
import me.richdev.FFA.Utils.LocationUtils;
import me.richdev.FFA.Utils.PacketsSenders;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;

public class KillListener implements Listener {

    private final List<Location> doorToKil = LocationUtils.getLocations(new Location(Bukkit.getWorld("Spawn"), -192D, 87D, -66D), new Location(Bukkit.getWorld("Spawn"), -192D, 96, -56D));

    private String getVisualhealth(double many, String who) {

        String visual = "§6Vida restante de §b§l"+who+" ";

        for (int i = 0; i < many / 2; i++) {
            visual = visual+"§4❤";
        }

        for (int i = 1; i < 10-(many / 2); i++) {
            visual = visual+"§7❤";
        }

        return visual;
    }

    @EventHandler
    public void damage(EntityDamageByEntityEvent e) {
        if(e.getEntity().equals(e.getDamager())) return;

        if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
            Player player = (Player) e.getEntity();
            GamePlayer gamePlayer = Main.getInstance().getPlayerHandler().getPlayer(player);

            Player killer = (Player) e.getDamager();
            GamePlayer k = Main.getInstance().getPlayerHandler().getPlayer(killer);


            gamePlayer.addHitter(k);
            gamePlayer.setLastHiter(k);

            new BukkitRunnable() {
                @Override
                public void run() {
                    PacketsSenders.sendActionBar(killer, getVisualhealth((player).getHealth(), player.getName()));
                }
            }.runTaskLater(Main.getInstance(), 3);

            if((player.getHealth() - e.getFinalDamage()) <= 0) {
                GamePlayerDeathEvent playerDeathEvent = new GamePlayerDeathEvent(gamePlayer, k, "");
                Bukkit.getPluginManager().callEvent(playerDeathEvent);
                e.setCancelled(true);
            }
        } else if (e.getDamager() instanceof Projectile && e.getEntity() instanceof Player) {
            Projectile projectile = (Projectile) e.getDamager();
            if (projectile.getShooter() instanceof Player) {
                Player shooter = (Player) projectile.getShooter();
                Player ded = (Player) e.getEntity();

                GamePlayer k = Main.getInstance().getPlayerHandler().getPlayer(shooter);
                GamePlayer d = Main.getInstance().getPlayerHandler().getPlayer(ded);

                d.addHitter(k);
                d.setLastHiter(k);

                PacketsSenders.sendActionBar(shooter, getVisualhealth((ded).getHealth(), ded.getName()));
                if((ded.getHealth() - e.getFinalDamage()) <= 0) {
                    GamePlayerDeathEvent playerDeathEvent = new GamePlayerDeathEvent(d, k, "");
                    Bukkit.getPluginManager().callEvent(playerDeathEvent);
                    e.setCancelled(true);
                }
            }
        } else if(e.getEntity() instanceof Player) {
            Player player = (Player) e.getEntity();
            GamePlayer gamePlayer = Main.getInstance().getPlayerHandler().getPlayer(player);
            if((player.getHealth() - e.getFinalDamage()) <= 0) {
                GamePlayerDeathEvent playerDeathEvent = new GamePlayerDeathEvent(gamePlayer, null, "");
                Bukkit.getPluginManager().callEvent(playerDeathEvent);
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void dead(GamePlayerDeathEvent e) {
        GamePlayer player = e.getDead();

        if (!player.getHitted().isEmpty()) {
            for (GamePlayer gamePlayer : player.getHitted()) {
                if (gamePlayer != player.getLastHiter() && gamePlayer != null && gamePlayer.isOnline()) {
                    gamePlayer.addAssist(player);
                    gamePlayer.getPlayer().playSound(gamePlayer.getPlayer().getLocation(), Sound.ORB_PICKUP, 10, 10);
                    //gamePlayer.getPlayer().sendMessage("§b§lASISTENCIA! §eMataste a §a"+player.getName());
                }
            }
            player.getHitted().clear();
        }

        /*
        GamePlayer last = player.getLastHiter();
        if (last != null && last.isOnline()) {
            last.addKill(player);
            PacketsSenders.sendActionBar(player.getLastHiter().getPlayer(), "§e§lKILL! §b§l"+player.getName());

            if (player.getSpecialGoldPrice() != 0) {
                last.getPlayer().sendMessage("§b§l"+player.getName()+" §etenía una recompensa especial, recibiste §6§l"+player.getSpecialGoldPrice()+" §eoro extra por matarlo.");
                last.setXP(last.getXP()+player.getSpecialGoldPrice());
                last.addGold(player.getSpecialGoldPrice());
            }

            Bukkit.broadcastMessage("§a§l"+player.getLastHiter().getName()+" §emató a §a§l"+player.getName());
            PacketsSenders.sendTitle(player.getPlayer(), "§cMUERTO", "§7Asesino: "+player.getLastHiter().getName(), 0, 33, 10);
            last.getPlayer().getInventory().addItem(items[4]);
        } else {
            Bukkit.broadcastMessage("§a§l" + player.getName() + " §emurió.");
        }*/

        GamePlayer last = e.getKiller();
        if (last != null && last.isOnline()) {
            last.addKill(player);
            //PacketsSenders.sendActionBar(player.getLastHiter().getPlayer(), "§e§lKILL! §b§l"+player.getName());

            if (player.getSpecialGoldPrice() != 0) {
                last.getPlayer().sendMessage("§b§l"+player.getName()+" §etenía una recompensa especial, recibiste §6§l"+player.getSpecialGoldPrice()+" §eoro extra por matarlo.");
                last.setXP(last.getXP()+player.getSpecialGoldPrice());
                last.addGold(player.getSpecialGoldPrice());
            }

            Bukkit.broadcastMessage("§a§l"+player.getLastHiter().getName()+" §emató a §a§l"+player.getName());
            PacketsSenders.sendTitle(player.getPlayer(), "§cMUERTO", "§7Asesino: "+player.getLastHiter().getName(), 0, 33, 10);
            last.getPlayer().getInventory().addItem(GamePlayer.items[4]);
            new BukkitRunnable() {
                @Override
                public void run() {
                    PacketsSenders.sendActionBar(player.getLastHiter().getPlayer(), "§e§lKILL! §b§l"+player.getName());
                }
            }.runTaskLaterAsynchronously(Main.getInstance(), 5);
        } else {
            Bukkit.broadcastMessage("§a§l" + player.getName() + " §emurió.");
        }

        player.addDead();
    }

    @EventHandler
    public void onPlayerChatEvent(AsyncPlayerChatEvent e) {
        GamePlayer fPlayer = Main.getInstance().getPlayerHandler().getPlayer(e.getPlayer());

        if (fPlayer != null) {
            e.setFormat("§8[§6Lv. "+fPlayer.getLEVEL()+"§8] §8["+fPlayer.getTitle().getColor()+fPlayer.getTitle().getTag()+"§8]§r "+e.getFormat());
            //e.setFormat("§8(§aLV. " + fPlayer.getLevel() + "§8) §8[§b" + fPlayer.getTitle().getColor() + fPlayer.getTitle().toString() + "§8]§r " + e.getFormat());
        }
    }

    /*
    @EventHandler
    public void goToCombat(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        if (doorToKil.contains(new Location(player.getWorld(), player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ()))) {
            player.getInventory().setArmorContents(armor);
            player.getInventory().setItem(0, items[0]);
            player.getInventory().setItem(1, items[5]);
            player.getInventory().setItem(2, items[1]);
            player.getInventory().setItem(4, items[3]);
            player.getInventory().setItem(5, items[4]);
            player.getInventory().setItem(8, items[2]);
        }
    }

    @EventHandler
    public void pressGold(PlayerInteractEvent e) {
        if (e.getAction() == Action.PHYSICAL) {
            if (e.getClickedBlock().getType() == Material.GOLD_PLATE) {
                Player player = e.getPlayer();
                player.getInventory().setArmorContents(armor);
                player.getInventory().setItem(0, items[0]);
                player.getInventory().setItem(1, items[5]);
                player.getInventory().setItem(2, items[1]);
                player.getInventory().setItem(4, items[3]);
                player.getInventory().setItem(5, items[4]);
                player.getInventory().setItem(8, items[2]);
            }


        }
    }

    @EventHandler
    public void respawn(PlayerRespawnEvent e) {
        Player player = e.getPlayer();
        player.getInventory().clear();
        player.getInventory().setArmorContents(armor);
        player.getInventory().setItem(0, items[0]);
        player.getInventory().setItem(1, items[5]);
        player.getInventory().setItem(2, items[1]);
        player.getInventory().setItem(4, items[3]);
        player.getInventory().setItem(5, items[4]);
        player.getInventory().setItem(8, items[2]);
    }
    */

    @EventHandler
    public void playerJoinEvent(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        player.teleport(new Location(Bukkit.getWorld("Spawn"), -198, 87, -60, -92, (float) 5.1));

        GamePlayer.setInventory(player, false);
    }


    @EventHandler
    public void drop(PlayerDropItemEvent e) {
        e.setCancelled(true);
    }


}