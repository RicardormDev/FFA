package me.richdev.FFA.Utils;

import me.richdev.FFA.Player.Leveling.PVPTitle;

public class TitleUtils {

    public static PVPTitle getBefore(PVPTitle title) {
        if(title.getOrder() == 0)
            return title;

        return PVPTitle.values()[title.getOrder() - 1];
    }

    public static PVPTitle getNext(PVPTitle title) {
        if(title.getOrder() + 1 == PVPTitle.values().length)
            return title;

        return PVPTitle.values()[title.getOrder() + 1];
    }

}
