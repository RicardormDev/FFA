package me.richdev.FFA.Utils;

public enum TeamAction {
    CREATE, DESTROY, UPDATE
}