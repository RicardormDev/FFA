package me.richdev.FFA.Utils.Validator;

import me.richdev.FFA.Utils.FastItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public abstract class Validator {

    private Inventory inv;
    private ItemStack a;
    private ItemStack b;

    /**
     *
     * @param player Jugador
     * @param title Titulo del cofre fake
     * @param yes Titulo del item si
     * @param lore Desc del item no
     */
    protected Validator(Player player, String title, String yes, String... lore) {
        Inventory inventory = Bukkit.createInventory(null, 9*3,  title);
        this.inv = inventory;

        ItemStack a = FastItemBuilder.getItem(yes, Material.STAINED_CLAY, 5, 1, null, lore);
        ItemStack b = FastItemBuilder.getItem("§cCancelar compra", Material.STAINED_CLAY, 14, 1, null, "§cCancelar compra");

        this.a = a;
        this.b = b;

        inventory.setItem(11, a);
        inventory.setItem(15, b);

        if(ValidatorListener.validatorMap.containsKey(player.getUniqueId())) ValidatorListener.validatorMap.remove(player.getUniqueId());
        ValidatorListener.validatorMap.put(player.getUniqueId(), this);
        player.openInventory(inventory);
    }

    public abstract void run();

    public Inventory getInv() {
        return inv;
    }

    public ItemStack getA() {
        return a;
    }

    public ItemStack getB() {
        return b;
    }
}
