package me.richdev.FFA.Utils.Validator;

import com.google.common.collect.Maps;
import me.richdev.FFA.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Map;
import java.util.UUID;

class ValidatorListener implements Listener{

    public static Map<UUID, Validator> validatorMap = Maps.newHashMap();

    static {
        Bukkit.getServer().getPluginManager().registerEvents(new ValidatorListener(), Main.getInstance());
    }

    @EventHandler
    public void click(InventoryClickEvent e) {
        if(validatorMap.containsKey(e.getWhoClicked().getUniqueId())) {
            Validator validator = validatorMap.get(e.getWhoClicked().getUniqueId());
            if(validator.getInv().getName().equals(e.getInventory().getName())) {

                Player player = (Player) e.getWhoClicked();
                if(e.getSlot() < 0) return;

                if(e.getCurrentItem() != null && e.getCurrentItem().getData().equals(validator.getA().getData())) {
                    validator.run();
                    player.playSound(player.getLocation(), Sound.LEVEL_UP, 10, 10);
                    validatorMap.remove(player.getUniqueId());

                    player.closeInventory();
                } else if(e.getCurrentItem() != null && e.getCurrentItem().equals(validator.getB())) {
                    validatorMap.remove(player.getUniqueId());
                    player.closeInventory();
                    player.sendMessage(ChatColor.RED + "Compra cancelada.");
                }

                e.setCancelled(true);
            }

        }
    }

    @EventHandler
    public void close(InventoryCloseEvent e) {
        if(validatorMap.containsKey(e.getPlayer().getUniqueId())) {
            Validator validator = validatorMap.get(e.getPlayer().getUniqueId());
            if(e.getInventory() == validator.getInv()) {
                validatorMap.remove(e.getPlayer().getUniqueId());
            }
        }
    }

    @EventHandler
    public void leave(PlayerQuitEvent e) {
        if(validatorMap.containsKey(e.getPlayer().getUniqueId())) {
            validatorMap.remove(e.getPlayer().getUniqueId());
        }
    }

}
