package me.richdev.FFA.Utils;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

public class PacketsSenders {

    public static void sendActionBar(Player who, String msg) {
        msg = ChatColor.translateAlternateColorCodes('&', msg);

        IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + msg + "\"}");
        PacketPlayOutChat packet = new PacketPlayOutChat(cbc, (byte)2);
        ((CraftPlayer) who).getHandle().playerConnection.sendPacket(packet);
    }

    public static void openChestAnimation(Location location, boolean action) {
        WorldServer worldServer = ((CraftWorld)location.getWorld()).getHandle();
        BlockPosition blockPosition = new BlockPosition(location.getX(), location.getY(), location.getZ());
        if(location.getBlock().getTypeId() == 54) {
            try {
                TileEntityChest tileEntityChest = (TileEntityChest) worldServer.getTileEntity(blockPosition);
                worldServer.playBlockAction(blockPosition, tileEntityChest.w(), 1, action ? 1 : 0);
            } catch (Exception e) {

            }
        }
    }

    public static void sendTitle(Player player, String title, String sub, int start, int stay, int finish) {
        IChatBaseComponent tc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}");
        IChatBaseComponent sc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + sub + "\"}");

        PacketPlayOutTitle pTi = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, tc);
        PacketPlayOutTitle pSub = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, sc);

        PacketPlayOutTitle pTime = new PacketPlayOutTitle(start, stay ,finish);

        CraftPlayer toSend = (CraftPlayer) player;

        toSend.getHandle().playerConnection.sendPacket(pTi);
        toSend.getHandle().playerConnection.sendPacket(pSub);
        toSend.getHandle().playerConnection.sendPacket(pTime);

    }

    public static void setPlayerListHeader(Player player,String header){
        CraftPlayer cplayer = (CraftPlayer) player;
        PlayerConnection connection = cplayer.getHandle().playerConnection;
        IChatBaseComponent hj = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + header + "\"}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
        try{
            Field headerField = packet.getClass().getDeclaredField("a");
            headerField.setAccessible(true);
            headerField.set(packet, hj);
            headerField.setAccessible(!headerField.isAccessible());

        } catch (Exception e){
            e.printStackTrace();
        }
        connection.sendPacket(packet);
    }


    public static void setPlayerListFooter(Player player,String footer) {
        CraftPlayer cp = (CraftPlayer) player;
        PlayerConnection con = cp.getHandle().playerConnection;
        IChatBaseComponent fj = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + footer + "\"}");
        PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
        try {
            Field footerField = packet.getClass().getDeclaredField("b");
            footerField.setAccessible(true);
            footerField.set(packet, fj);
            footerField.setAccessible(!footerField.isAccessible());
        } catch (Exception e) {
            e.printStackTrace();
        }
        con.sendPacket(packet);
    }

    public static void sendList(Player player, String a, String b) {
        setPlayerListHeader(player, a);
        setPlayerListFooter(player, b);
    }
}
