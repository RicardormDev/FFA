package me.richdev.FFA.Utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.codec.binary.Base64;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class FastItemBuilder {

    @SuppressWarnings("deprecation")
    public static ItemStack getItem(String name, Material material, int data, int amount, Enchant[] enchantment, String... lore) {
        ItemStack item = new ItemStack(material, amount, (short)0, (byte)data);
        ItemMeta meta = item.getItemMeta();

        if(name != null)
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));

        if(lore != null) {
            List<String> convertedLore = new ArrayList<>();
            for(String s : lore)
                convertedLore.add(ChatColor.translateAlternateColorCodes('&', s));
            meta.setLore(convertedLore);
        }



        if(enchantment != null)
            for(Enchant e : enchantment)
                meta.addEnchant(e.enchantment, e.level, true);

        item.setItemMeta(meta);
        return item;
    }

    @SuppressWarnings("deprecation")
    public static ItemStack getItem(String name, Material material, int data, int amount, List<Enchant> enchantment, List<String> lore) {
        ItemStack item = new ItemStack(material, amount, (short)0, (byte)data);
        ItemMeta meta = item.getItemMeta();
        if(name != null)
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));

        List<String> convertedLore = new ArrayList<>();
        if(lore != null)
            for(String s : lore)
                convertedLore.add(ChatColor.translateAlternateColorCodes('&', s));
        if(lore != null)
            meta.setLore(convertedLore);

        if(enchantment != null)
            for(Enchant e : enchantment)
                meta.addEnchant(e.enchantment, e.level, true);

        item.setItemMeta(meta);
        return item;
    }

    @SuppressWarnings("deprecation")
    public static ItemStack getSkullItem(String name, Material material, int data, int amount, List<Enchant> enchantment, List<String> lore, String skullOwner) {
        if(material != Material.SKULL_ITEM) {
            throw new IllegalArgumentException(material.toString() + " cannot be cast as SKULL_ITEM");
        }

        ItemStack item = new ItemStack(material, amount, (short)0, (byte)data);
        SkullMeta meta = (SkullMeta) item.getItemMeta();
        if(name != null)
            meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));

        List<String> convertedLore = new ArrayList<>();
        if(lore != null)
            for(String s : lore)
                convertedLore.add(ChatColor.translateAlternateColorCodes('&', s));
        if(lore != null)
            meta.setLore(convertedLore);

        if(enchantment != null)
            for(Enchant e : enchantment)
                meta.addEnchant(e.enchantment, e.level, true);

        if(skullOwner != null) {
            GameProfile profile = new GameProfile(UUID.randomUUID(), skullOwner);

            String url = skullOwner;

            //Validate.notEmpty(base64, "Base64 no puede estar vacio.");
            //Validate.notEmpty(signature, "Signature no puede estar vacio.");

            byte[] encodedData = Base64.encodeBase64(String.format("{textures:{SKIN:{url:\""+ skullOwner + "\"}}}", url).getBytes());

            profile.getProperties().put("textures", new Property("textures",
                    new String(encodedData)));

            Field field;

            try {
                field = meta.getClass().getDeclaredField("profile");
                field.setAccessible(true);
                field.set(meta, profile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        item.setItemMeta(meta);
        return item;
    }

    public static class Enchant {
        private int level;
        private Enchantment enchantment;

        public Enchant(int level, Enchantment enchantment) {
            this.level = level;
            this.enchantment = enchantment;
        }

        public int getLevel() {
            return level;
        }

        public Enchantment getEnchantment() {
            return enchantment;
        }
    }
}
