package me.richdev.FFA.GUIS;

import me.richdev.FFA.Main;
import me.richdev.FFA.Player.GamePlayer;
import me.richdev.FFA.Utils.Validator.Validator;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

public class GuiListener implements Listener {

    @EventHandler
    public void shopNoP(InventoryClickEvent e) {
        if(!e.getInventory().getName().equals(ChatColor.GOLD + "Tienda no permamente")) return;
        if(e.getSlot() < 0) return;

        ItemStack item = e.getCurrentItem();
        if(item == null || item.getType() == Material.AIR || !item.hasItemMeta() || !item.getItemMeta().hasDisplayName()) return;

        GamePlayer player = Main.getInstance().getPlayerHandler().getPlayer((Player) e.getWhoClicked());
        if(player == null) return;

        switch (e.getCurrentItem().getType()) {
            case DIAMOND_SWORD:

                if(player.getGold() >= 267) {

                    new Validator(player.getPlayer(), "Comprar espada", "§aAceptar compra", "§7Comprar espada por 267 de oro") {
                        @Override
                        public void run() {
                            player.removeGold(267);
                            player.getPlayer().getInventory().setItem(0, GamePlayer.special[2]);
                            player.getPlayer().getInventory().remove(Material.IRON_SWORD);
                            player.getPlayer().sendMessage("§6Compra finalizada.");
                            player.getPlayer().playSound(player.getPlayer().getLocation(), Sound.LEVEL_UP, 10, 10);
                        }
                    };
                } else {
                    player.getPlayer().closeInventory();
                    player.getPlayer().sendMessage("§cNo tienes suficiente oro");
                }

                break;

            case DIAMOND_CHESTPLATE:
                if(player.getGold() >= 438) {

                    new Validator(player.getPlayer(), "Comprar peto", "§aAceptar compra", "§7Comprar espada por 438 de oro") {
                        @Override
                        public void run() {
                            player.removeGold(438);
                            player.getPlayer().getInventory().setChestplate(GamePlayer.special[0]);
                            player.getPlayer().getInventory().remove(Material.IRON_CHESTPLATE);
                            player.getPlayer().sendMessage("§6Compra finalizada.");
                            player.getPlayer().sendMessage("§7Armadura equipada.");
                            player.getPlayer().playSound(player.getPlayer().getLocation(), Sound.LEVEL_UP, 10, 10);
                        }
                    };
                } else {
                    player.getPlayer().closeInventory();
                    player.getPlayer().sendMessage("§cNo tienes suficiente oro");
                }
                break;

            case DIAMOND_BOOTS:
                if(player.getGold() >= 314) {

                    new Validator(player.getPlayer(), "Comprar botas", "§aAceptar compra", "§7Comprar espada por 314 de oro") {
                        @Override
                        public void run() {
                            player.removeGold(314);
                            player.getPlayer().getInventory().setBoots(GamePlayer.special[1]);
                            player.getPlayer().getInventory().remove(Material.IRON_BOOTS);
                            player.getPlayer().sendMessage("§6Compra finalizada.");
                            player.getPlayer().sendMessage("§7Armadura equipada.");
                            player.getPlayer().playSound(player.getPlayer().getLocation(), Sound.LEVEL_UP, 10, 10);
                        }
                    };
                } else {
                    player.getPlayer().closeInventory();
                    player.getPlayer().sendMessage("§cNo tienes suficiente oro");
                }
                break;
        }
        e.setCancelled(true);
    }
}
