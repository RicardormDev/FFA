package me.richdev.FFA.GUIS;

import me.richdev.FFA.Player.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class GuiSaver {

    public static void openGui(GamePlayer player) {
        Inventory inventory = Bukkit.createInventory(null, 9*3, ChatColor.GOLD + "Tienda no permamente");

        ItemStack a = new ItemStack(Material.DIAMOND_SWORD);
        ItemMeta ma = a.getItemMeta();
        ma.setDisplayName("§6Comprar espada de diamante");
        ma.setLore(Arrays.asList("§7La perderas al morir.", "§7Costo: §6$267"));
        a.setItemMeta(ma);

        ItemStack a2 = new ItemStack(Material.DIAMOND_CHESTPLATE);
        ItemMeta ma2 = a.getItemMeta();
        ma2.setDisplayName("§6Comprar Peto de Diamante");
        ma2.setLore(Arrays.asList("§7La perderas al morir.", "§7Costo: §6$438"));
        a2.setItemMeta(ma2);

        ItemStack a3 = new ItemStack(Material.DIAMOND_BOOTS);
        ItemMeta ma3 = a.getItemMeta();
        ma3.setDisplayName("§6Comprar Botas de Diamante");
        ma3.setLore(Arrays.asList("§7La perderas al morir.", "§7Costo: §6$314"));
        a3.setItemMeta(ma3);

        inventory.setItem(10, a);
        inventory.setItem(13, a2);
        inventory.setItem(16, a3);
        player.getPlayer().openInventory(inventory);
    }
}
