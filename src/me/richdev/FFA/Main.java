package me.richdev.FFA;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import me.richdev.FFA.GUIS.GuiListener;
import me.richdev.FFA.KillManager.GamePlayerDeathEvent;
import me.richdev.FFA.KillManager.KillListener;
import me.richdev.FFA.Player.GamePlayer;
import me.richdev.FFA.Player.GamePlayerEvents;
import me.richdev.FFA.Player.PlayerHandler;
import me.richdev.FFA.Tops.TopAssists;
import me.richdev.FFA.Tops.TopDeads;
import me.richdev.FFA.Tops.TopKills;
import me.richdev.FFA.Tops.TopLevel;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;
import java.util.Random;

public class Main extends JavaPlugin{

    private static Plugin leaderheads;
    private static Main instance;
    public boolean PlaceHoldersActive;
    private Random random;
    private SQLHandler sqlHandler;
    private PlayerHandler playerHandler;

    @Override
    public void onEnable() {
        instance = this;
        this.random = new Random();
        this.playerHandler = new PlayerHandler();
        this.sqlHandler = new SQLHandler();

        try {
            sqlHandler.openConenction();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        Bukkit.getServer().getPluginManager().registerEvents(new GamePlayerEvents(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new KillListener(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new GuiListener(), this);

        leaderheads = Bukkit.getPluginManager().getPlugin("LeaderHeads");
        if (leaderheads != null) {
            new TopKills();
            new TopLevel();
            new TopDeads();
            new TopAssists();
        }

        if (Bukkit.getPluginManager().isPluginEnabled("MVdWPlaceholderAPI")) {
            this.PlaceHoldersActive = true;

            PlaceholderAPI.registerPlaceholder(this, "FFAKills", (e) -> {
                if (e.isOnline()) {
                    return ""+playerHandler.getPlayer(e.getPlayer()).getKills();
                }
                return "nope";
            });

            PlaceholderAPI.registerPlaceholder(this, "FFALevel", (e) -> {
                if (e.isOnline()) {
                    return ""+playerHandler.getPlayer(e.getPlayer()).getLEVEL();
                }
                return "nope";
            });

            PlaceholderAPI.registerPlaceholder(this, "FFADeads", (e) -> {
                if (e.isOnline()) {
                    return ""+playerHandler.getPlayer(e.getPlayer()).getDeads();
                }
                return "nope";
            });

            PlaceholderAPI.registerPlaceholder(this, "FFAAssists", (e) -> {
                if (e.isOnline()) {
                    return ""+playerHandler.getPlayer(e.getPlayer()).getAssists();
                }
                return "nope";
            });
        }

        for (Player player : Bukkit.getOnlinePlayers()) {
            GamePlayer a = playerHandler.getPlayer(player);
            if(a == null) {

                int playerID = Main.getInstance().getSqlHandler().getID(player.getUniqueId());

                if(!Main.getInstance().getSqlHandler().playerExists(playerID)) {
                    Main.getInstance().getSqlHandler().createPlayer(playerID);
                }

                a = new GamePlayer(player, playerID);
            }

            playerHandler.registerPlayer(a);

            GamePlayer.setInventory(a.getPlayer(), false);
        }

        getCommand("ffa").setExecutor(new Commands());
    }

    @Override
    public void onDisable() {
        for (GamePlayer player : playerHandler.getAll()) {
            Main.getInstance().getSqlHandler().savePlayerNotAsync(player);
        }
    }

    public static Main getInstance() {
        return instance;
    }

    public int getNumberBetweenTwo(int min, int max) {
        return random.nextInt((max - min) + 1) + min;
    }

    public SQLHandler getSqlHandler() {
        return sqlHandler;
    }

    public PlayerHandler getPlayerHandler() {
        return playerHandler;
    }
}
