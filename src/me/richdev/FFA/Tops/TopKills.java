package me.richdev.FFA.Tops;

import me.richdev.FFA.Main;
import me.richdev.FFA.Player.PlayerHandler;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class TopKills extends OnlineDataCollector{

    public TopKills() {
        super("TKills", "FullPVP", BoardType.DEFAULT, "&3&lTOP KILLS", "tkills", Arrays.asList(null, null, "&3{amount} Kills", null));
    }

    @Override
    public Double getScore(Player player) {
        return (double) Main.getInstance().getPlayerHandler().getPlayer(player).getKills();
    }

}
