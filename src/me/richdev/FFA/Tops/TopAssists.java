package me.richdev.FFA.Tops;

import me.richdev.FFA.Main;
import me.richdev.FFA.Player.PlayerHandler;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class TopAssists extends OnlineDataCollector{
    public TopAssists() {
        super("TAssists", "FullPVP", BoardType.DEFAULT, "&3&lTOP ASISTENCIAS", "tassists", Arrays.asList(null, null, "&3{amount}", "&3asistencias"));
    }

    @Override
    public Double getScore(Player player) {
        return (double)  Main.getInstance().getPlayerHandler().getPlayer(player).getAssists();
    }
}
