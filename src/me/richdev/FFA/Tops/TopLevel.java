package me.richdev.FFA.Tops;

import me.richdev.FFA.Main;
import me.richdev.FFA.Player.PlayerHandler;
import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class TopLevel extends OnlineDataCollector{

    public TopLevel() {
        super("TopLevel", "FullPVP", BoardType.DEFAULT, "&3&lTOP LEVELS", "TLevel", Arrays.asList(null, null, "&3{amount} Level", null));
    }

    @Override
    public Double getScore(Player player) {
        return (double) Main.getInstance().getPlayerHandler().getPlayer(player).getLEVEL();
    }
}
