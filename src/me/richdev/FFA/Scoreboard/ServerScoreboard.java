package me.richdev.FFA.Scoreboard;

import me.richdev.FFA.Main;
import me.richdev.FFA.Player.GamePlayer;

import java.util.Arrays;
import java.util.List;

public interface ServerScoreboard {

    GamePlayer getPlayer();

    List<String> getLines();

    void putLine(String text);

    void buildToPlayer();

    void update();

    static void removeScoreboardFromPlayer(GamePlayer player) {
        player.getPlayer().setScoreboard(Main.getInstance().getServer().getScoreboardManager().getNewScoreboard());
    }

    static List<String> getTitleAnimations() {
        return Arrays.asList(
                "§6§lFFA" , "§6§lFFA" , "§6§lFFA" , "§6§lFFA",  "§6§lFFA" , "§6§lFFA" , "§6§lFFA" , "§6§lFFA"
                , "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA"
                , "§6§lFFA" , "§6§lFFA" , "§6§lFFA" , "§6§lFFA",  "§6§lFFA" , "§6§lFFA" , "§6§lFFA" , "§6§lFFA"
                , "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA"
                , "§6§lFFA" , "§6§lFFA" , "§6§lFFA" , "§6§lFFA",  "§6§lFFA" , "§6§lFFA" , "§6§lFFA" , "§6§lFFA"
                , "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA"
                , "§6§lFFA" , "§6§lFFA" , "§6§lFFA" , "§6§lFFA",  "§6§lFFA" , "§6§lFFA" , "§6§lFFA" , "§6§lFFA"
                , "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA", "§f§lFFA"
        );
    }

}
