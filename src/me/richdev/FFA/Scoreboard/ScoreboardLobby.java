package me.richdev.FFA.Scoreboard;

import me.richdev.FFA.Main;
import me.richdev.FFA.Player.GamePlayer;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import java.util.ArrayList;
import java.util.List;

public class ScoreboardLobby implements ServerScoreboard{

    private List<String> lines;

    private ScoreboardManager manager;
    private Scoreboard scoreboard;
    private Objective objective;

    private GamePlayer player;

    public ScoreboardLobby(GamePlayer player){
        this.lines = new ArrayList<>();
        this.player = player;
        manager = Main.getInstance().getServer().getScoreboardManager();
        scoreboard = manager.getNewScoreboard();
        objective = scoreboard.registerNewObjective("FFAScore", "");
        objective.setDisplayName("");

        lines.add("§1");
        lines.add("§fNivel: §a" + player.getLEVEL());
        lines.add("§fXP Restante: §a" + player.getXPRequiredToNewLevel());
        lines.add("§3");
        lines.add("§fKillstreak: §a" + player.getKillstreak());
        lines.add("§7");
        lines.add("§fKills: §a" +player.getKills());
        lines.add("§fMuertes: §a" + player.getDeads());
        lines.add("§fOro: §6" + player.getGold());
        lines.add("§5");
        lines.add("§ewww.volkmc.net");

        player.setScoreboard(this);
        buildToPlayer();
    }

    @Override
    public GamePlayer getPlayer() {
        return player;
    }

    @Override
    public List<String> getLines() {
        return lines;
    }

    @Override
    public void putLine(String text) {
        lines.add(text);
    }

    @Override
    public void buildToPlayer() {
        new BukkitRunnable() {
            List<String> ss = ServerScoreboard.getTitleAnimations();
            int i = 0;
            @Override
            public void run() {

                if(player == null || player.getScoreboard() != ScoreboardLobby.this) {
                    cancel();
                }

                if(i == ss.size()) {
                    i = 0;
                }

                objective.setDisplayName(ss.get(i));
                i++;
            }
        }.runTaskTimer(Main.getInstance(), 0, 1);

        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        int c = lines.size();
        for(String lines : lines)  {
            objective.getScore(lines).setScore(c);
            c--;
        }

        player.getPlayer().setScoreboard(scoreboard);
    }

    @Override
    public void update() {
        int c = lines.size();
        for(String lines : lines)  {
            objective.getScore(lines).setScore(c);
            c--;
        }

        player.getPlayer().setScoreboard(scoreboard);
    }

}
